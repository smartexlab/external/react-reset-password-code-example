import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import api from 'common/new-api';

import { setCustomer } from 'main/common/actions/auth';
import { createNotification } from 'main/common/actions/notifications';

import ResetPassword from './component';

export const mapDispatchToProps = (dispatch) => {
    return {
        api: api(dispatch),
        setCustomer: bindActionCreators(setCustomer, dispatch),
        createNotification: bindActionCreators(createNotification, dispatch)
    };
};

export default connect(null, mapDispatchToProps)(withRouter(ResetPassword));
