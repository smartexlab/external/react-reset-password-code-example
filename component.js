import React from 'react';
import PropTypes from 'prop-types';
import get from 'lodash/get';

import routes from 'common/routes';
import constants from 'common/constants';

import utils from 'main/common/utils';
import Input from 'main/controls/input';
import Button from 'main/controls/button';
import HyperLink from 'main/controls/hyper-link';
import ValidateForm from 'main/components/validate-form';
import ValidateGroup from 'main/components/validate-group';
import ValidateField from 'main/components/validate-field';
import ValidateSubmit from 'main/components/validate-submit';
import ValidateMessage from 'main/components/validate-message';

import './index.scss';

import iconBackgroundContent from 'icons/authentication-background.png';

const baseClassName = 'reset-password';

class ResetPassword extends React.PureComponent {
    constructor(props) {
        super(props);

        const params = new URLSearchParams(props.location.search);

        this.state = {
            token: params.get('token'),
            email: params.get('email'),
            password: '',
            waiting: false
        };
    }

    getClassNames = () => {
        return {
            component: baseClassName,
            componentImage: `${baseClassName}__image`,
            componentImageIcon: `${baseClassName}__image-icon`,
            content: `${baseClassName}__content`,
            password: `${baseClassName}__password`,
            confirmation: `${baseClassName}__confirmation`,
            resetPassword: `${baseClassName}__reset-password`,
            signIn: `${baseClassName}__sign-in`
        };
    };

    render() {
        const { api } = this.props;
        const { token, email, waiting } = this.state;

        const classNames = this.getClassNames();
        const requestData = {
            token,
            email
        };

        return (
            <div className={classNames.component}>
                <div className={classNames.componentImage}>
                    <img className={classNames.componentImageIcon} src={iconBackgroundContent} alt="" />
                </div>

                <div className={classNames.content}>
                    <h1>Change password</h1>

                    <ValidateForm
                        url={api.routes.passwordReset}
                        requestData={requestData}
                        waiting={waiting}
                        onSuccess={this.handleResetPasswordSuccess}
                    >
                        <ValidateGroup>
                            <div className={classNames.password}>
                                <ValidateField fieldName="password" required={true}>
                                    <Input
                                        type="password"
                                        placeholder="New password"
                                        title="New password"
                                        maxLength={50}
                                    />
                                </ValidateField>
                                <ValidateMessage fieldName="password" />
                            </div>

                            <div className={classNames.confirmation}>
                                <ValidateField fieldName="password_confirmation" required={true}>
                                    <Input
                                        type="password"
                                        placeholder="New password confirmation"
                                        title="New password confirmation"
                                        maxLength={50}
                                        onChanged={this.handlePasswordChange}
                                    />
                                </ValidateField>
                                <ValidateMessage fieldName="password_confirmation" />
                            </div>
                        </ValidateGroup>

                        <div className={classNames.resetPassword}>
                            <ValidateSubmit>
                                <Button theme="transparent dark">
                                    CHANGE PASSWORD
                                </Button>
                            </ValidateSubmit>
                        </div>
                    </ValidateForm>

                    <div className={classNames.signIn}>
                        <HyperLink to={routes.signIn} theme="border">
                            Return to Login
                        </HyperLink>
                    </div>
                </div>
            </div>
        );
    }

    handlePasswordChange = (password) => {
        this.setState({
            password
        });
    };

    handleResetPasswordSuccess = () => {
        const {
            api,
            setCustomer,
            history,
            createNotification
        } = this.props;
        const { email, password } = this.state;

        const requestData = {
            email,
            password,
            client_secret: constants.CLIENT_SECRET,
            client_id: constants.CLIENT_ID,
            remember_me: true
        };

        this.setState({
            waiting: true
        });

        api.login(requestData)
            .then((response) => {
                const accessToken = get(response, 'auth_token.access_token');

                utils.setLoginCookie(accessToken);

                return api.getCurrentCustomer();
            }).then((customer) => {
                setCustomer(customer);

                const message = 'Your password has been reset.';
                const isVerified = get(customer, 'account.is_verified', false);

                createNotification({
                    text: message,
                    type: 'success'
                });

                if (isVerified) {
                    history.push(routes.myOrders);
                } else {
                    history.push(routes.customer);
                }
            });
    };
}

ResetPassword.propTypes = {
    api: PropTypes.shape({
        login: PropTypes.func.isRequired,
        routes: PropTypes.shape({
            passwordReset: PropTypes.string.isRequired
        }).isRequired
    }).isRequired,
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
    }).isRequired,
    location: PropTypes.shape({
        search: PropTypes.string
    }).isRequired,
    setCustomer: PropTypes.func.isRequired,
    createNotification: PropTypes.func.isRequired
};

export default ResetPassword;
