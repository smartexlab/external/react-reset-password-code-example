import React from 'react';
import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import ResetPassword from './component';
import routes from 'common/routes';

const propsRequired = {
    history: {
        push: jest.fn()
    },
    location: {
        search: 'test'
    },
    createNotification: jest.fn()
};

const propsDefault = {
    ...propsRequired
};

describe('Should render ResetPassword component', () => {
    test('without default properties', () => {
        const component = shallow(<ResetPassword {...propsDefault}/>);
        const output = shallowToJson(component);

        expect(output).toMatchSnapshot();
    });

    test('with required properties', () => {
        const component = shallow(<ResetPassword {...propsRequired}/>);
        const output = shallowToJson(component);

        expect(output).toMatchSnapshot();
    });

    describe('render component', () => {
        test('when showLoader is equal true', () => {
            const component = shallow(<ResetPassword {...propsDefault}/>);
            const instance = component.instance();

            instance.state = {
                showLoader: true
            };

            instance.forceUpdate();

            const output = shallowToJson(component);

            expect(output).toMatchSnapshot();
        });

        test('when showLoader is equal false', () => {
            const component = shallow(<ResetPassword {...propsDefault}/>);
            const instance = component.instance();

            instance.state = {
                showLoader: false
            };

            instance.forceUpdate();

            const output = shallowToJson(component);

            expect(output).toMatchSnapshot();
        });
    });
});

describe('ResetPassword component', () => {
    test('handleResetPasswordSuccess method', () => {
        const component = shallow(<ResetPassword {...propsDefault}/>);
        const instance = component.instance();

        instance.handleResetPasswordSuccess();

        expect(instance.props.createNotification).toBeCalled();
        expect(instance.props.history.push).toBeCalledWith(routes.myOrders);
    });
});
