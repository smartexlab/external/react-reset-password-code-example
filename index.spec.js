import { mapDispatchToProps } from './index';

describe('Should render mapDispatchToProps function', () => {
    test('mapDispatchToProps function', () => {
        const dispatch = jest.fn();
        const mapDispatch = mapDispatchToProps(dispatch);

        expect(mapDispatch.createNotification).toBeInstanceOf(Function);
    });
});
